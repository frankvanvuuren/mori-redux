module.exports = {
  entry: './index.js',
  output: {
    path: './dist',
    filename: 'mori-redux.js'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015']
      }
    }]
  }
}
