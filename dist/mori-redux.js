'use strict';

var _require = require('mori');

var toClj = _require.toClj;
var isMap = _require.isMap;
var hasKey = _require.hasKey;
var get = _require.get;
var hashMap = _require.hashMap;
var assoc = _require.assoc;
var reduceKV = _require.reduceKV;


exports.createReducer = function (initialState, actionHandlers) {
  var actionHandlersMap = isMap(actionHandlers) ? actionHandlers : toClj(actionHandlers);
  return function (previousState, action) {
    var state = previousState !== undefined ? previousState : initialState;
    if (action === undefined) return state;
    return hasKey(actionHandlersMap, action.type) ? get(actionHandlersMap, action.type)(state, action) : state;
  };
};

exports.combineReducers = function (reducersMap) {
  var initialState = reduceKV(function (acc, key, val) {
    return assoc(acc, key, val());
  }, hashMap(), reducersMap);
  return function (previousState, action) {
    var state = previousState !== undefined ? previousState : initialState;
    return reduceKV(function (acc, key, val) {
      return assoc(acc, key, val(get(state, key), action));
    }, hashMap(), reducersMap);
  };
};

exports.logMiddleware = function (store) {
  return function (next) {
    return function (action) {
      console.group(action.type);
      console.log('dispatching', action);
      var result = next(action);
      console.log('next state', store.getState().toString());
      console.groupEnd();
      return result;
    };
  };
};

