const test = require('tape')

const { hashMap, list, map, curry } = require('mori')

const add = (a, b) => a + b

const { createReducer } = require('../index')

test('createReducer', t => {
  t.plan(8)

  const initialState = list(1, 2, 3)
  const actionHandlers = hashMap('increase', (state, { by }) => map(curry(add, by), state))
  const reducer = createReducer(initialState, actionHandlers)
  const action = {'type': 'increase', 'by': 3}
  const notMatchingAction = hashMap('type', 'doest_not_exist')

  t.equal(
    typeof createReducer, 'function',
    'should be a function'
  )

  t.equal(
    createReducer.length, 2,
    'should have an arity of 2'
  )

  t.equal(
    typeof reducer, 'function',
    'should return a function'
  )

  t.equal(
    reducer.length, 2,
    'should return a function with an arity of 2'
  )

  t.equal(
    reducer(undefined, undefined).toString(), initialState.toString(),
    'return function should return previous state if called without previous state or matching actionHandlers'
  )

  t.equal(
    reducer(list('a', 'b', 'c'), notMatchingAction).toString(), '("a" "b" "c")',
    'return function should return previous state if no actionHandlers matches'
  )

  t.equal(
    reducer(undefined, action).toString(), '(4 5 6)',
    'return function should return new state if actionHandler matches'
  )

  t.equal(
    reducer().toString(), '(1 2 3)',
    'return function should return initial state if passed no paramaters'
  )
})
